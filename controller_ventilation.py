__author__ = 'proky.hb'

import gpio as GPIO

class ControllerVentilation:
    ventilator = GPIO.RELAY_5

    def process(self, pinState):
        GPIO.output(self.ventilator, int(pinState))
