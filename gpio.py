import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)


#gpio relay output pins
RELAY_1 = 17
RELAY_2 = 18
RELAY_3 = 22
RELAY_4 = 23
RELAY_5 = 24
RELAY_6 = 25

#gpio input pins
INPUT_1 = 9;
INPUT_2 = 10;
INPUT_3 = 11;

#init mode
GPIO.setup(RELAY_1, GPIO.OUT)
GPIO.setup(RELAY_2, GPIO.OUT)
GPIO.setup(RELAY_3, GPIO.OUT)
GPIO.setup(RELAY_4, GPIO.OUT)
GPIO.setup(RELAY_5, GPIO.OUT)
GPIO.setup(RELAY_6, GPIO.OUT)

GPIO.setup(INPUT_1, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(INPUT_2, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(INPUT_3, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

#default values
GPIO.output(RELAY_1, True)
GPIO.output(RELAY_2, True)
GPIO.output(RELAY_3, True)
GPIO.output(RELAY_4, True)
GPIO.output(RELAY_5, True)
GPIO.output(RELAY_6, True)

def input(pin):
    return GPIO.input(pin)

def output(pin, value):
    GPIO.output(pin, value)

def cleanup():
    GPIO.cleanup()