import json

__author__ = 'proky.hb'

import urllib
import urllib2

import temp_sensors
import services


class RestApi:

    setStateEndPoint = "http://topenipj.l01v04.komtesa.com/index.php?com_heating&task=rest.setState"
    getStateEndPoint = "http://topenipj.l01v04.komtesa.com/index.php?com_heating&task=rest.getState"

    def postState(self):

        payload = urllib.urlencode([
            ("sensors[TANK_HEATING]", services.read_sensor_state(services.tank_heating)),
            ("sensors[FIREPLACE_PUMP]", services.read_sensor_state(services.fireplace_pump)),
            ("sensors[HEATING]", services.read_sensor_state(services.heating)),
            ("temperatures[TANK_1]", temp_sensors.read_temp(temp_sensors.tank_1)),
            ("temperatures[TANK_2]", temp_sensors.read_temp(temp_sensors.tank_2)),
            ("temperatures[TANK_3]", temp_sensors.read_temp(temp_sensors.tank_3)),
            ("temperatures[RETURN_TEMP]", temp_sensors.read_temp(temp_sensors.return_temp)),
            ("temperatures[OUTPUT_TEMP]", temp_sensors.read_temp(temp_sensors.output_temp)),
            ("temperatures[OUTDOOR_TEMP]", temp_sensors.read_temp(temp_sensors.fireplace_out)),
            ("temperatures[FIREPLACE_OUT]", temp_sensors.read_temp(temp_sensors.fireplace_out)),
            ("temperatures[FIREPLACE_INNER_IN]", temp_sensors.read_temp(temp_sensors.fireplace_inner_in)),
            ("temperatures[FIREPLACE_OUTER_IN]", temp_sensors.read_temp(temp_sensors.fireplace_outer_in))
        ])

        r = urllib2.urlopen(url=self.setStateEndPoint, data=payload)


    def getState(self):

        r = urllib2.urlopen(url=self.getStateEndPoint)
        data = json.load(r)

        return data


