import gpio as GPIOPins
import RPi.GPIO as GPIO

tank_heating = GPIOPins.INPUT_1
fireplace_pump = GPIOPins.INPUT_2
heating = GPIOPins.INPUT_3

def read_sensor_state(sensor):
    gpioState = GPIO.input(sensor);
    if gpioState == True:
        return 1
    return 0