__author__ = 'proky.hb'

import gpio as GPIO

class ControllerOut:
    pump = GPIO.RELAY_1

    def process(self, pinState):
        GPIO.output(self.pump, int(pinState))
