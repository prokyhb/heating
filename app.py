#!/usr/bin/env python

import time
import gpio as GPIO
import signal
import sys
from controller_out import ControllerOut
from controller_in import ControllerIn
from controller_ventilation import ControllerVentilation
from rest_api import RestApi

controller_out = ControllerOut()
controller_in = ControllerIn()
controller_ventilation = ControllerVentilation()
rest_api = RestApi()

counter = 60
reset_counter = 60


def signal_term_handler(signal, frame):
    print('OMG, I\'m killed')
    GPIO.cleanup()
    sys.exit(0)


signal.signal(signal.SIGTERM, signal_term_handler)

try:
    while True:

        # counter
        if counter == reset_counter:
            # post current state
            rest_api.postState()

            # get current state
            data = rest_api.getState()

            # setup io
            controller_out.process(data['tank_out'])
            controller_in.process(data['tank_in'])
            controller_ventilation.process(data['ventilation'])

            counter = 1

        else:
            counter += 1

        time.sleep(1)

except KeyboardInterrupt:
    print("Manual Exit")
except RuntimeError as e:
    print("Oops, something went wrong".format(e.errno, e.strerror))
finally:
    GPIO.cleanup()
