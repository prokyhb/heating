__author__ = 'proky.hb'

import gpio as GPIO

class ControllerIn:
    relay = GPIO.RELAY_4

    def process(self, pinState):
        GPIO.output(self.relay, int(pinState))
