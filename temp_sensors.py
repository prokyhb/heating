import os
import os.path

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

base_dir = '/sys/bus/w1/devices/'

# device IDs
sensor1_directory = base_dir + '28-000004f10876'
sensor2_directory = base_dir + '28-00000522c2aa'
sensor3_directory = base_dir + '28-000004f2192b'
sensor4_directory = base_dir + '28-000004f17e99'
sensor5_directory = base_dir + '28-000004f15491'
sensor6_directory = base_dir + '28-0000052270f5'
sensor7_directory = base_dir + '28-000006739dfa'
sensor8_directory = base_dir + '28-000006da14a2'
sensor9_directory = base_dir + '28-000006d99b76'

# topeni
return_temp = sensor1_directory + '/w1_slave'
output_temp = sensor2_directory + '/w1_slave'

# venkovni teplota
outdoor_temp = sensor6_directory + '/w1_slave'

# tank
tank_1 = sensor5_directory + '/w1_slave'
tank_2 = sensor4_directory + '/w1_slave'
tank_3 = sensor3_directory + '/w1_slave'

# vlozka
fireplace_outer_in = sensor7_directory + '/w1_slave'
fireplace_inner_in = sensor8_directory + '/w1_slave'
fireplace_out = sensor9_directory + '/w1_slave'


def read_temp_raw(file):
    f = open(file, 'r')
    lines = f.readlines()
    f.close()
    return lines


def read_temp(file):
    if not os.path.exists(file):
        return 0

    lines = read_temp_raw(file)
    while lines[0].strip()[-3:] != 'YES':
        lines = read_temp_raw(file)
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos + 2:]
        temp_c = float(temp_string) / 1000.0
        return temp_c
